'use strict'

require('picturefill')
require('intersection-observer')

import HoverClass from './lib/HoverClass'
import HoverOpacity from './lib/HoverOpacity'
import Smooth from './lib/Smooth'
import TelGrant from './lib/TelGrant'
import EqualHeight from './lib/EqualHeight'
import Drawer from './lib/Drawer'
import Svg from './lib/Svg'
import ScrollAnimation from './lib/ScrollAnimation'
import DropDown from './lib/DropDown'
import Swiper from './lib/Swiper'
// import ShowPagetop from './lib/ShowPagetop'
// import Zipcode from './lib/Zipcode';

new HoverClass()
new HoverOpacity()
new Smooth()
new TelGrant()
new EqualHeight('.js-max_height')
new Drawer()
new Svg()
new ScrollAnimation()
new DropDown()
new Swiper()
// new ShowPagetop()
// new Zipcode();
