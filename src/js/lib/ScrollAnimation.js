export default class {
  constructor() {
    const boxes = document.querySelectorAll('.js-anim_elm')
    const boxesArray = Array.prototype.slice.call(boxes, 0)
    const options = {
      root: null,
      rootMargin: '-35% 0px',
      threshold: 0,
    }
    const observer = new IntersectionObserver(doWhenIntersect, options)

    boxesArray.forEach(function (box) {
      observer.observe(box)
    })
    function doWhenIntersect(entries) {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.classList.add('is-act')
        }
      })
    }
  }
}
