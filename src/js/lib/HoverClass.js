import Ua from './Ua'

export default class {
  constructor() {
    const ua = new Ua()
    const _ua = ua.uaCheck()

    if (!_ua.Tablet && !_ua.Mobile) {
      const targetList = document.querySelectorAll('.js-hover')
      const enter = (e) => {
        e.target.classList.add('is-hover')
      }
      const leave = (e) => {
        e.target.classList.remove('is-hover')
      }
      const node = Array.prototype.slice.call(targetList, 0)
      node.forEach(function (target) {
        target.addEventListener('mouseenter', enter)
        target.addEventListener('mouseleave', leave)
      })
    }
  }
}
