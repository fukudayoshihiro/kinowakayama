import Swiper from 'swiper/bundle'

export default class {
  constructor() {
    let maxSlideNum = 0,
      thisSlideIndex = 0
    new Swiper('.swiper-container', {
      loop: true,
      spaceBetween: 0,
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true,
      },
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
      },
      on: {
        init: function () {
          // Swiper起動時に実行される
          let slideNum = [] 
          let slideLength = document.querySelectorAll('.swiper-container .swiper-slide').length
          if (this.loopedSlides) {
            for (let i = 0; i < slideLength; i++) {
              slideNum.push(document.querySelector('.swiper-container').getElementsByClassName('swiper-slide')[i].dataset.swiperSlideIndex)
              maxSlideNum = Math.max(...slideNum) + 1
            }
          } else {
            maxSlideNum = slideLength
          }
          // スライドの最大枚数を出力
          document.querySelector('.js-Maxnum').innerHTML = maxSlideNum
          // 最初の数値を出力（スライドの初期位置が違う場合は、ここを変更する）
          document.querySelector('.js-currentNum').innerHTML = 1
        },
        transitionStart: function () {
          thisSlideIndex =
            [].slice.call(document.querySelector('.swiper-pagination').querySelectorAll('.swiper-pagination-bullet')).indexOf(document.querySelector('.swiper-pagination-bullet-active')) + 1
          document.querySelector('.js-currentNum').innerHTML = thisSlideIndex
        },
      },
    })
  }
}
