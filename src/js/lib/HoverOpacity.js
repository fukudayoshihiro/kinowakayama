import { gsap } from 'gsap'

export default class {
  constructor() {
    const targetList = document.querySelectorAll('.hover')
    const enter = () => {
      gsap.to('.hover', 1, { autoAlpha: 0.4 })
    }
    const leave = () => {
      gsap.to('.hover', 1, { autoAlpha: 1 })
    }
    const node = Array.prototype.slice.call(targetList, 0)
    node.forEach(function (target) {
      target.addEventListener('mouseenter', enter)
      target.addEventListener('mouseleave', leave)
    })
  }
}
