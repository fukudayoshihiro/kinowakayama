import { gsap } from 'gsap'
export default class {
  constructor() {
    const hamburger = document.getElementById('js-hamburger')
    const drawer = document.getElementById('js-drawer')
    const body = document.body

    hamburger.addEventListener('click', (event) => {
      let $this = event.currentTarget
      $this.querySelector('span').classList.toggle('hidden')
      $this.classList.toggle('is-act')

      if ($this.classList.contains('is-act')) {
        body.classList.add('is-act')
        animation(drawer, 'open')
      } else {
        body.classList.remove('is-act')
        animation(drawer, 'close')
      }
    })

    const animation = (el, type) => {
      if (type == 'open') {
        gsap.to(el, 0.3, { autoAlpha: 1, display: 'block' })
      } else {
        gsap.to(el, 0.3, { autoAlpha: 0, display: 'none' })
      }
    }
  }
}
