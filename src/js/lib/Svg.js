import 'desvg'
export default class {
  constructor() {
    document.addEventListener('DOMContentLoaded', function () {
      deSVG('.js-svg', true)
    })
  }
}
