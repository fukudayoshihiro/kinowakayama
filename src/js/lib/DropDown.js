export default class {
  constructor() {
    const megaTrgList = document.querySelectorAll('.js-mega_trg')
    const megaTrg = Array.prototype.slice.call(megaTrgList, 0)
    let moveTimer = 0
    let hideTimer = 0
    megaTrg.forEach(function (target) {
      target.addEventListener('mouseenter', (event) => {
        // タイマーの解除
        clearTimeout(moveTimer)
        clearTimeout(hideTimer)
        // タイマーのセット(マウスが外れて○秒後にhideを削除)
        moveTimer = setTimeout(function () {
          target.classList.add('is-act')
          target.lastElementChild.classList.add('is-act')
        }, 100)
      })
      target.addEventListener('mouseleave', (event) => {
        // タイマーの解除
        clearTimeout(moveTimer)
        // タイマーのセット(マウスが外れて○秒後にhideを追加)
        hideTimer = setTimeout(function () {
          target.classList.remove('is-act')
          target.lastElementChild.classList.remove('is-act')
        }, 100)
      })
    })
  }
}
